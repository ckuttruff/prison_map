--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: csv_data; Type: TABLE; Schema: public; Owner: slackz
--

CREATE TABLE csv_data (
    category text,
    campaign text,
    color text,
    name text,
    city text,
    state text,
    photo_url text,
    date text,
    description text,
    amount_divested text,
    potential_divestment text,
    lat double precision,
    long double precision,
    weight double precision
);


ALTER TABLE csv_data OWNER TO slackz;

--
-- Data for Name: csv_data; Type: TABLE DATA; Schema: public; Owner: slackz
--

COPY csv_data (category, campaign, color, name, city, state, photo_url, date, description, amount_divested, potential_divestment, lat, long, weight) FROM stdin;
University	Victory	#bf8040	Georgetown University	Washington	D.C.	\N	2017-10-01	Georgetown Students for a Radically Ethical Endowment (GU F.R.E.E.) pushed the university to state that going forward the university will not own investments in private prison companies and will encourage its external investment managers to avoid investments in these companies.	\N	\N	38.907192000000002	-77.036871000000005	0.299999999999999989
University	Launch	#bf8040	UCSA reIGNITE	Santa Barbara	CA	\N	2017-10-01	University of California Student Association launches reIGNITE campaign to raise prison divestment awareness, push for affirmative action, establish a UC-wide UC Police Department oversight committee and create a student bill of rights at a university-wide level.	\N	\N	34.4208309999999997	-119.698189999999997	0.299999999999999989
University	Launch	#bf8040	Kenyon College	Gambier	OH	\N	2017-09-01	DivestKenyon is pushing the college Board of Trustees to divest from private prisons and fossil fuel companies. 	\N	\N	40.3756190000000004	-82.3971039999999988	0.299999999999999989
University	Launch	#bf8040	NYU 	New York City	NY	\N	2017-03-01	NYU Prison Divest is pushing the university to divest from private prison companies and to refuse to renew contracts with Aramark.	\N	\N	40.7127750000000006	-74.0059729999999973	0.299999999999999989
University	Launch	#bf8040	MIT 	Cambridge	MA	\N	2017-05-01	MIT students launch campaign to demand that Aramark-- that profits from prisons and exploits incarcerated people-- is banned from campus as a meal provider.	\N	\N	42.3736159999999984	-71.1097340000000031	0.299999999999999989
Legislation	Launch	#00a3cc	Bill Introduced to Stop OR Pension Investments in Prisons & Their Investors	Salem	OR	\N	2017-04-01	Oregon Senator Kathleen Taylor introduced Senate Bill 1005 to divest the Oregon Public Employees Retirement System from private prisons and the Million Shares Club, aiming to turn an Oregon Education Association's resolution into law.	\N	\N	44.9428979999999996	-123.035095999999996	0.299999999999999989
City	Victory	#cc0000	Alameda Divests from Prisons & DAPL	Alameda	CA	\N	2017-02-01	The City of Alameda divested $36 million from its accounts at Wells Fargo for the bank's role in financing the Dakota Access Pipeline and private prisons.	$36,000,000	\N	37.7652059999999992	-122.241636	0.299999999999999989
City	Launch	#cc0000	New Haven 	New Haven	CT	\N	2017-08-01	Mayor Toni Harper stated she would seek to move out of Wells Fargo for their financing of DAPL and prisons.	\N	$11,000,000	41.3082739999999973	-72.9278840000000059	0.299999999999999989
University	Launch	#bf8040	University of Southern Florida	Tampa	FL	\N	2017-04-01	USF Divest won a student referendum to Divest the university's foundation From Fossil Fuels, Private Prison and Companies Complicit in Human Rights Violations. USF Divest continues to pressure the universiy president to enact the student referendum. 	\N	\N	27.9505750000000006	-82.457177999999999	0.299999999999999989
University	Launch	#bf8040	Amerherst College	Amherst	MA	\N	2017-10-01	Direct Action Coordinating Committee (DACC) launched a campaign to divest the college’s endowment from fossil fuels and indirect investments in private prisons.	\N	\N	42.3732219999999984	-72.5198539999999952	0.299999999999999989
City	Launch	#cc0000	Cincinatti City Council Pushes Pension Divestment	Cincinatti	OH	\N	2017-08-01	Cincinnati City Council members are asking the city's pension board to divest from companies that operate or do business with private prisons.	\N	$2.5 million	39.6417629999999974	-77.7199930000000023	0.299999999999999989
University	Launch	#bf8040	Reed College	Portland	OR	\N	2017-10-01	Reedies Against Racism have held rallies, disrupted humanities lectures, led marches through campus, occupied offices, slept in hallways, and put up posters denouncing the universities investments in Wells Fargo for its ties to private prison companies and the Dakota Access Pipeline.	\N	200,000,000	45.523062000000003	-122.676481999999993	0.299999999999999989
University	Launch	#bf8040	Yale	New Haven	CT	\N	2017-04-01	Yale Students for Prison Divestment demand that Yale divest from the fossil fuel and private prison industries and eliminate the SIC.	\N	\N	41.3082739999999973	-72.9278840000000059	0.299999999999999989
University	Launch	#bf8040	CSU Long Beach	Long Beach	CA	https://www.facebook.com/CSULBDivest/photos/a.221088068381925.1073741830.218351495322249/222330394924359/?type=3&theater	2017-05-01	CSULB Divest pushed the Student Senate to pass divestment bills targeting companies that profit from the private prison industry, from the Israeli occupation of Palestine and from oppression of the LGBTQ+ community.	\N	\N	33.7700499999999977	-118.193738999999994	0.299999999999999989
University	Launch	#bf8040	Vanderbilt Student Senate Recommends Vanderbilt Divest from Private Prisons	Nashville	TN	\N	2017-03-01	Vanderbilt Student Senate voted to recommend the administration divest from funding private prisons, and recognize its historical ties to CCA. 	\N	\N	36.1626639999999995	-86.7816020000000066	0.299999999999999989
\N	\N	\N	\N	\N	\N	\N	\N	\N	$4,109,516,000	\N	\N	\N	\N
Legislation	Launch	#00a3cc	Dignity for Detained Immigrants Act	Washington	D.C.	\N	2017-10-01	Dignity for Detained Immigrants Act, introduced by Reps Pramila Jayapal & Adam Smith (D-WA) and 50 co-sponsors would phase out the use of private prisons for immigrant detainees over a three-year period, and increase transparency and oversight of immigrant detention facilities.	\N	\N	38.907192000000002	-77.036871000000005	0.299999999999999989
Legislation	Launch	#00a3cc	Private Prison Information Act	Washington	D.C.	\N	2017-08-01	Senate Bill 1728, introduced by Sen. Benjamin Cardin (D-MD), would subject federal for-profit prisons to the same disclosure requirements as publicly-run federal facilities. 	\N	\N	38.907192000000002	-77.036871000000005	0.299999999999999989
Movement	Launch	#008000	No Justice No Pride	Washington	D.C.	\N	2017-06-01	LGBTQ immigrant and people of color activists pressure Capital Pride and the Human Rights Campaign to cut ties with Wells Fargo over prisons, abuse of trans workers, and foreclosure of Black communities.	\N	\N	38.907192000000002	-77.036871000000005	0.299999999999999989
City	Victory	#cc0000	Portland Divests from Prisons, DAPL, & the Israeli Occupation of Palestine	Portland	OR	\N	2017-04-01	The City of Portland ended all corporate investments due to intersectional organizing for prison divestment, indigenous rights in Palestine and the U.S., and fossil fuel divestment. The City commited to terminate its banking contract with Wells Fargo and to conduct a feasibilty study to create a municipal bank.	$539,000,000	\N	45.523062000000003	-122.676481999999993	0.699999999999999956
City	Victory	#cc0000	NYC Pension Fully Divests from Private Prisons	New York City	NY	\N	2017-06-01	New York City’s pension system was the first in the nation to fully divest from private prisons, dumping about $48 million worth of stock and bonds from GEO Group, CoreCivic Inc. (CCA) and G4S.	$48,000,000	\N	40.7127750000000006	-74.0059729999999973	0.400000000000000022
Movement	Launch	#008000	AFL-CIO Passes Resolution to Resist Private Prisons	St. Louis	MO	\N	2017-10-01	At its 2017 convention, the AFL-CIO adopted Resolution 25 that resolves, "AFL-CIO, its affiliates and state federations promote federal, state and local legislation, policies and practices that end the for-profit pipeline of correctional facilities or services."	\N	\N	38.627003000000002	-90.1994040000000012	0.299999999999999989
Legal	Victory	#df9fbf	SCOTUS Rules in Favor of Government Transparency Against Private Prison Corporations	Washington	D.C.	\N	2017-10-01	The Supreme Court denied a petition by private prison corporations seeking to block the release of government documents about their immigration detention practices. The Freedom of Information Act case was originally brought by the Center for Constitutional Rights (CCR) and Detention Watch Network (DWN). The Supreme Court's decision let stand the federal district court ruling in July 2016, that the government must release details of its contracts with private prison corporations.	\N	\N	39.0256649999999965	-77.0763670000000047	0.299999999999999989
University	Victory	#bf8040	UC Divests from Wells Fargo	San Diego	CA	\N	2017-01-01	University of California terminates $475 million worth of contracts with Wells Fargo due to powerful organizing by Afrikan Black Coalition supported by Enlace. The University of California (UC) will \nretract a $300 million line of credit and a $150 million interest reset \ncontract with Wells Fargo by April of this year. $200 million has \nalready been retracted, and the remaining $100 million will be divested \nafter a replacement bank is found. This follows the termination of the \n$25 million commercial paper contract with Wells Fargo by the UC in \nNovember of 2016. 	$475,000,000	\N	32.7157380000000018	-117.161084000000002	0.880000000000000004
University	Launch	#bf8040	Harvard Students Launch Divestment Campaign	Cambridge	MA	\N	2017-04-01	Harvard College Project for Justice is calling for Harvard to divest from “direct and indirect investments in private prison stock, as well as publicly commit to a moratorium on such investments in the future.”	\N	23,000,000	42.3736159999999984	-71.1097340000000031	0.299999999999999989
University	Victory	#bf8040	Stanford University Victory & Next Steps	Stanford	CA	\N	2017-10-01	SU Prison Divest pressured the University to publicly state that they have no prison investments, and to initiate a full review of the Investment Responsibility policy and procedures. SU Prison Divest continues to pressure University to divest from prison investors and profiteers.	\N	\N	37.4263979999999989	-122.168360000000007	0.299999999999999989
University	Launch	#bf8040	Tufts Divests from G4S & Israeli Military & Prison Companies	Medford	MA	\N	2017-05-01	Tufts Community Union Senate voted to encourage the university divest from G4S and other companies for their ties to Israeli military and prison technology.	\N	\N	42.4184300000000007	-71.1061640000000068	0.299999999999999989
City	Launch	#cc0000	Washington D.C.	Washington	D.C.	\N	2017-03-01	City Councilors introduced a resolution calling for an end to banking with Wells Fargo over investment in the Dakota Access Pipeline, its lending to private prisons\n and high-profile payouts to settle race discrimination and customer \nservice allegations.	\N	\N	38.907192000000002	-77.036871000000005	0.299999999999999989
Movement	Launch	#008000	Interfaith Bank Boycott Campaign	Washington	D.C.	\N	2017-10-01	Interfaith Immigration Coalition launched boycott campaign of Wells Fargo and Bank of America, for their role in investing in prisons and anti-immigrant and incarceration policies, until a clean DREAM Act is passed by Congress.	\N	\N	39.6417629999999974	-77.7199930000000023	0.299999999999999989
City	Launch	#cc0000	Minneapolis	Minneapolis	MN	\N	2017-07-01	Minneapolis City Council members are calling for JPMorgan Chase to sever its ties with the Trump administration and divest from private prisons and immigration detention centers.	\N	\N	44.9777529999999999	-93.2650110000000012	0.299999999999999989
Movement	Victory	#008000	#RevoketheRainbow	Washington	D.C.	\N	2017-05-01	GetEQUAL pushed the National LGBTQ Task Force to reject corporate sponsorship from Wells Fargo across the organization, including the annual Creating Change conference. However, National LGBTQ TaskForce is retaining Wells Fargo as the presenting sponsor of the Task Force Gala-Miami.	\N	\N	39.6417629999999974	-77.7199930000000023	0.200000000000000011
Movement	Launch	#008000	Interfaith Bank Boycott Campaign	Washington	D.C.	\N	2017-10-01	Interfaith Immigration Coalition launched boycott campaign of Wells Fargo and Bank of America, for their role in investing in prisons and anti-immigrant and incarceration policies, until a clean DREAM Act is passed by Congress.	\N	\N	39.6417629999999974	-77.7199930000000023	0.299999999999999989
City	Victory	#cc0000	Seattle Divests from Prisons & DAPL	Seattle	WA	\N	2017-02-01	The City of Seattle divested $3 billion from major prison lender and Dakota Access Pipeline financier, Wells Fargo.	$3,000,000,000	\N	47.6062089999999998	-122.332070999999999	1.10000000000000009
University	Launch	#bf8040	University of Maryland	College Park	MD	\N	2016-11-01	ProtectUMD student coalition issued demands to divest from prison labor and the prison industrial complex.	\N	\N	38.9896969999999996	-76.9377599999999973	0.299999999999999989
University	Victory	#bf8040	Princeton	Princeton	NJ	\N	2017-04-01	Princeton Private Prison Divest (PPPD) pressured the university vice president to state the endowment has not and will not invest in private prisons, and that a resolution to make that binding may still be brought forward.	\N	\N	40.3572980000000001	-74.667223000000007	0.299999999999999989
University	Launch	#bf8040	University of Illinois	Urbana-Champaign	IL	\N	2017-02-01	UIUC Divest is calling upon the university to divest from corporations that directly profit from human rights violations, including G4S and Sodexho. Their targets are complicit in human rights violations from Palestine to the US and everywhere in between. 	\N	\N	40.1019519999999972	-88.2271609999999953	0.299999999999999989
Legislation	Victory	#00a3cc	Dignity not Detention	Sacramento	CA	\N	2017-10-01	Responding to years of organizing by immigrant communities, California Governor Brown signed into law SB 29, the Dignity Not Detention Bill. The Dignity Not Detention Bill is a critical step towards ending for-profit immigrant detention in California, and provides a model for other states to follow. 	\N	\N	38.5815720000000013	-121.494399999999999	0.299999999999999989
City	Victory	#cc0000	Berkeley Furthers Wells Fargo Divestment & Socially Responsible Investing Policy	Berkeley	CA	\N	2017-05-01	The City Council moved forward next steps in divestment from Wells Fargo and the development of new criteria for socially responsible investing and banking.	$10,356,000	\N	37.8715929999999972	-122.272746999999995	0.200000000000000011
City	Victory	#cc0000	Philly Divests from Private Prisons	Philadelphia	PA	\N	2017-10-01	Philadelphia Board of Pensions and Retirement divest from private prison companies CoreCivic, GEO Group, and G4S.	$1,160,000	\N	39.9525840000000017	-75.165222	0.100000000000000006
\.


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

